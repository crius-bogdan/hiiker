(function(doc){

    const mobBtn = getElem('.menu__mob_btn'),
          manu = getElem('.menu'),
          reviewSliders = doc.querySelectorAll('.rev-sl'),
          // Here i get Popup
          modalClimat = getElem('.mod-clim'),
          // Here i get Popup titles
          modalClimatTitle = getElems('.md-title'),
          // Here i get Popup buttons https://prnt.sc/pzdcmb
        //   Each header has an owl data-climat which says what type of climate it belongs to.
          modalBtms = getElems('.mod-btn'),
          // It is overlay :)
          overlay = getElem('.overlay'),
          mobImage = getElems('[data-mob-src]'),
          /*
            with the help of this variable I get element classes for further work with them.
            You may ask why then you need a variable? !!!
            I will answer that when compiling, the variable takes the form https://prnt.sc/pzdd7m. 
            Which in turn is much shorter than CLASSLIST.
            Exactly the same principle works DATASET :)
          */
          clas = 'classList',
          data = 'dataset';

    // if(window.innerWidth < 1000 && mobImage) {
    //     for (let i = 0; i < mobImage.length; i++) {
    //         let mobSrc = mobImage[i][data].mobSrc;
    //         mobImage[i].src = mobSrc
    //     }
    // }
    doc.addEventListener('DOMContentLoaded', function(e){
        svg4everybody({});

    });
    const trailSlider = new Swiper('.slider-trail', {
        // slidesPerView: 3,
        spaceBetween: 37,
        loop: true,
        navigation: {
            prevEl: '.popular__btn_prev',
            nextEl: '.popular__btn_next',
        },
        breakpoints: {
            996: {
                slidesPerView: 3,
            },
            767: {
                slidesPerView: 2,
            },
            // 320: {
            //     slidesPerView: 'auto',
            // }
        }
    });
    const featuredSlider = new Swiper('.slider-featured', {
        slidesPerView: 'auto',
        spaceBetween: 28,
        loop: true,
        navigation: {
            prevEl: '.featured__btn_prev',
            nextEl: '.featured__btn_next',
        },
    });
    const trailInfoSliderImg = new Swiper('.sl-trail-img', {
        // slidesPerView: 1,
        loop: true,
        // effect: 'fade',
        navigation: {
            prevEl: '.trail-info__btn_prev',
            nextEl: '.trail-info__btn_next',
        },
    });
    if (reviewSliders) {
        reviewSliders.forEach((el) => {
            const trailInfoSliderImg = new Swiper(el, {
                slidesPerView: 'auto',
                spaceBetween: 27,
                loop: true,
                navigation: {
                    // prevEl: '.trail-info__btn_prev',
                    nextEl: '.reviews__btn_next',
                },
            }); 
        });
    }
    doc.addEventListener('click', function(e){
        let target = e.target;

        while(target != this) {

            if (match(target, 'menu__mob')) {
                // console.log(true)
                mobBtn[clas].toggle('active');
                manu[clas].toggle('open');
            }
            if(match(target, 'cl-item')) {
                alert(`Look at app/main.js file for more information :)`);
                // We got to the most interesting

                /*
                    Each button that initiates the opening of a pop-up has a DATA-CLIMAT attribute.
                    He, in order to determine what should be the title of the pop-up. 
                    Remember the variable modalClimatTitle, then I will explain what it is for.
                */
                let btnClimat = target[data].climat;
                overlay[clas].add('show'); // Showing the Overlay
                modalClimat[clas].add('open'); // Opening pop-up

                /*
                    Our variable modalClimatTitle contains these elements https://prnt.sc/pzdflg.
                    Each item has a data-climat and data-show attribute.
                    data-climat Shows what climate it refers to, as you remember, the button that initiated the event has the same attribute, and if they match, then we belong to the attribute
                    data-show add the value * true * and in CSS there is a rule that will show this header if the value is * true *
                */
                for (let i = 0; i < modalClimatTitle.length; i++) {
                    if (modalClimatTitle[i][data].climat == btnClimat) {
                        modalClimatTitle[i][data].show = true;
                    }
                }

                /*
                    Each button https://prnt.sc/pzdh4j which also has several attributes https://prnt.sc/pzdher

                    This attribute is data-modal-type, in order to find out what weather the pop-up window is open for.

                    This data-option attribute shows what kind of button it is (what exactly it means for which data it is intended) we will work with this attribute a little lower

                    Here we add the value from the button attribute to this data-modal-type attribute.
                */
                for (let i = 0; i < modalBtms.length; i++) {
                    modalBtms[i][data].modalType = btnClimat;
                }
            }
            if(match(target, 'overlay') || match(target, 'mod-cl')) {
                // Here, all values are reset to zero.
                overlay[clas].remove('show');
                modalClimat[clas].remove('open');
                for (let i = 0; i < modalClimatTitle.length; i++) {
                    modalClimatTitle[i][data].show = false;
                }
                for (let i = 0; i < modalBtms.length; i++) {
                    modalBtms[i][clas].remove('active');
                    modalBtms[i][data].modalType = '';
                }
            }
            if(match(target, 'mod-btn')) {
                /*
                    When you click on any of the buttons https://prnt.sc/pzdiub.

                    Events occur in that order.
                    1. We take the values from the data-option to the option variable
                    2. Switch is launched in which the values are checked and in some of the blocks you can add some kind of Ajax request for drawing charts.
                
                    I did all this just so as not to produce too much HTML. And so that the popup can be used many times later for other purposes.
                
                */
                let option = target[data].option;
                function addClass() {
                    for (let i = 0; i < modalBtms.length; i++) {
                        if(modalBtms[i][data].option == option) {
                            modalBtms[i][clas].toggle('active');
                            continue;
                        } else if (modalBtms[i][data].option != option) {
                            modalBtms[i][clas].remove('active');
                        }
                    }
                }
                switch (option) {
                    case 'avg-temp':
                        alert(`${option}, Look at app/main.js file for more information :)`);
                        addClass();
                        break;

                    case 'max-temp':
                        alert(`${option}, Look at app/main.js file for more information :)`);
                        addClass();
                        break;

                    case 'min-humidity':
                        alert(`${option}, Look at app/main.js file for more information :)`);
                        addClass();
                        break;

                    case 'avg-wind-daily':
                        alert(`${option}, Look at app/main.js file for more information :)`);
                        addClass(target);
                        break;

                    case 'solar':
                        alert(`${option}, Look at app/main.js file for more information :)`);
                        addClass();
                        break;

                    case 'min-temp':
                        alert(`${option}, Look at app/main.js file for more information :)`);
                        addClass();
                        break;

                    case 'precipitation':
                        alert(`${option}, Look at app/main.js file for more information :)`);
                        addClass();
                        break;

                    case 'max-humidity':
                        alert(`${option}, Look at app/main.js file for more information :)`);
                        addClass();
                        break;

                    case 'max-wind-daily':
                        alert(`${option}, Look at app/main.js file for more information :)`);
                        addClass();
                        break;
                
                    default:
                        break;
                }
            }

            target = target.parentNode;
        }
    });

    function match(el, cls) {
        return el[clas].contains(cls);
    }
    function getElem(el) {
        return doc.querySelector(el);
    }
    function getElems(el) {
        return doc.querySelectorAll(el);
    }
    // console.log('hello')
})(document);